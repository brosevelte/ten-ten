RUN SERVER
uvicorn src.app.server.asgi:app --reload

RUN UI
npm run dev

Model Input
1. 0 * 100 -> The board (binary, we don't care about the types of pieces played)
2. 20 * 3 -> The pieces in hand (19 + 1 null)


Model Output [5, 3 softmax, 100 softmax]
 1. 0 0 0 -> The piece to place
 2. 0 * 100   -> The position to put it (origin)


Pointing cleared rows (triangular number):
    1r0c: 10 pt 

    2r0c: 30 pt

    3r0c: 60 pt

    4r0c: 100 pt
    0r4c: 100 pt

    5r0c: 150 pt

    5r1c: 210 pt


Type of ML:
 * RL: Have to create some "value" for each stage and the expect value of stages after a move
    * Is this really true? Can I not have a game be the "trajectory"
 * Evolutionary: Have to evaluate the "life" of the model.



Evo:
1. Get parents
    * Parents can be same to show they are surviving
2. Make babies
    * Splice parents genes into children
    * If parents are the same just return the parent
3. Mutation
    * Randomly change genes
    * If survivor do nothing