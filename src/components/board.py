from typing import List

import numpy as np

from .point import Point
from .pieces import Piece


class Board:
    def __init__(self, n: int):
        self.n = n
        self.board = np.zeros((n, n), dtype=np.int8)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.n})"

    def __str__(self):
        board = []
        for i in range(self.board.shape[0]):
            row = []
            for j in range(self.board.shape[1]):
                row.append(str(int(self.board[i, j])))
            board.append("  ".join(row))
        return "\n".join(board)

    def is_in_bounds(self, origin):
        return self.n > origin.x >= 0 and self.n > origin.y >= 0

    def can_add_piece(self, origin: Point, piece: Piece):
        if piece is None:
            return False, "Attempting to place null Piece."

        if not self.is_in_bounds(origin):
            return False, "Attempting to place Piece outside bounds."

        for offset in piece.shape:
            location = origin + offset

            if not self.is_in_bounds(location):
                return False, "Attempting to place Piece not entirely in bounds."

            if self.board[location.y, location.x] > 0:
                return False, "Attempting to place Piece on already occupied space."

        return True, ""

    def add_piece(self, origin: Point, piece: Piece):
        for offset in piece.shape:
            location = origin + offset
            self.board[location.y, location.x] = piece.color

    def get_full_columns(self):
        return np.where(np.count_nonzero(self.board, axis=0) == self.n)[0]

    def get_full_rows(self):
        return np.where(np.count_nonzero(self.board, axis=1) == self.n)[0]

    def clear_columns(self, columns: List[int]):
        self.board[:, columns] = 0

    def clear_rows(self, rows: List[int]):
        self.board[rows, :] = 0
