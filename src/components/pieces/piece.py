from typing import List

from src.components.point import Point


class Piece:
    def __init__(self, shape: List[Point], color: int):
        self.shape = shape
        self.color = color

    def render(self):
        piece_board = [[0 for _ in range(5)] for _ in range(5)]

        for point in self.shape:
            piece_board[point.y][point.x] = self.color

        return piece_board

    def __repr__(self):
        return f"{self.__class__.__name__}()"

    def __str__(self):
        piece_board = self.render()

        return "\n".join(
            ["  ".join([str(elem) for elem in row]) for row in piece_board]
        )

    @classmethod
    def get_all_pieces(cls):
        def recursive_search(cur_class):
            sub_classes = cur_class.__subclasses__()

            for sub_class in sub_classes:
                if not sub_class.__name__.startswith("_"):
                    all_pieces.append(sub_class)
                recursive_search(sub_class)

        all_pieces = []
        recursive_search(cls)
        return all_pieces


class PieceFactory:
    @classmethod
    def get_piece_lut(cls):
        return {piece.__name__: piece for piece in Piece.get_all_pieces()}

    @classmethod
    def get_piece_index_lut(cls):
        return {piece.__name__: i for i, piece in enumerate(Piece.get_all_pieces())}

    @classmethod
    def new(cls, piece_name):
        piece_lut = cls.get_piece_lut()
        return piece_lut[piece_name]()
