from src.components.point import Point
from src.components.pieces import Piece


class _Line(Piece):
    def __init__(self, length, horizontal: bool, color):
        if horizontal:
            shape = [Point(x, 0) for x in range(length)]
        else:
            shape = [Point(0, y) for y in range(length)]

        super().__init__(shape=shape, color=color)


class _TwoLine(_Line):
    def __init__(self, horizontal):
        super().__init__(length=2, horizontal=horizontal, color=1)


class _ThreeLine(_Line):
    def __init__(self, horizontal):
        super().__init__(length=3, horizontal=horizontal, color=2)


class _FourLine(_Line):
    def __init__(self, horizontal):
        super().__init__(length=4, horizontal=horizontal, color=3)


class _FiveLine(_Line):
    def __init__(self, horizontal):
        super().__init__(length=5, horizontal=horizontal, color=4)


class OneByTwoLine(_TwoLine):
    def __init__(self):
        super().__init__(horizontal=True)


class TwoByOneLine(_TwoLine):
    def __init__(self):
        super().__init__(horizontal=False)


class OneByThreeLine(_ThreeLine):
    def __init__(self):
        super().__init__(horizontal=True)


class ThreeByOneLine(_ThreeLine):
    def __init__(self):
        super().__init__(horizontal=False)


class OneByFourLine(_FourLine):
    def __init__(self):
        super().__init__(horizontal=True)


class FourByOneLine(_FourLine):
    def __init__(self):
        super().__init__(horizontal=False)


class OneByFiveLine(_FiveLine):
    def __init__(self):
        super().__init__(horizontal=True)


class FiveByOneLine(_FiveLine):
    def __init__(self):
        super().__init__(horizontal=False)
