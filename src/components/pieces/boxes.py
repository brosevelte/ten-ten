from src.components.point import Point
from src.components.pieces import Piece


class _Box(Piece):
    def __init__(self, length, color):
        shape = [Point(x, y) for x in range(length) for y in range(length)]

        super().__init__(shape=shape, color=color)


class OneBox(_Box):
    def __init__(self):
        super().__init__(length=1, color=5)


class TwoBox(_Box):
    def __init__(self):
        super().__init__(length=2, color=6)


class ThreeBox(_Box):
    def __init__(self):
        super().__init__(length=3, color=7)
