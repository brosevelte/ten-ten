from src.components.point import Point
from src.components.pieces import Piece


class _TwoCorner(Piece):
    def __init__(self, shape):
        super().__init__(shape=shape, color=8)


class _ThreeCorner(Piece):
    def __init__(self, shape):
        super().__init__(shape=shape, color=9)


class TwoCornerBottomLeft(_TwoCorner):
    def __init__(self):
        super().__init__(shape=[Point(0, 0), Point(0, 1), Point(1, 1)])


class TwoCornerBottomRight(_TwoCorner):
    def __init__(self):
        super().__init__(shape=[Point(0, 1), Point(1, 1), Point(1, 0)])


class TwoCornerTopLeft(_TwoCorner):
    def __init__(self):
        super().__init__(shape=[Point(0, 0), Point(0, 1), Point(1, 0)])


class TwoCornerTopRight(_TwoCorner):
    def __init__(self):
        super().__init__(shape=[Point(0, 0), Point(1, 0), Point(1, 1)])


class ThreeCornerBottomLeft(_ThreeCorner):
    def __init__(self):
        super().__init__(
            shape=[Point(0, 0), Point(0, 1), Point(0, 2), Point(1, 2), Point(2, 2)]
        )


class ThreeCornerBottomRight(_ThreeCorner):
    def __init__(self):
        super().__init__(
            shape=[Point(0, 2), Point(1, 2), Point(2, 2), Point(2, 1), Point(2, 0)]
        )


class ThreeCornerTopLeft(_ThreeCorner):
    def __init__(self):
        super().__init__(
            shape=[Point(0, 0), Point(0, 1), Point(0, 2), Point(1, 0), Point(2, 0)]
        )


class ThreeCornerTopRight(_ThreeCorner):
    def __init__(self):
        super().__init__(
            shape=[Point(0, 0), Point(1, 0), Point(2, 0), Point(2, 1), Point(2, 2)]
        )
