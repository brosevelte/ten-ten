import typing

from fastapi import APIRouter
import numpy as np
from pydantic import BaseModel

from src.game import Game
from src.components.pieces import PieceFactory

game = Game("App")
game_router = APIRouter(prefix="/game")


class GameState(BaseModel):
    score: int
    board: typing.List[typing.List[int]]
    hand: typing.List[typing.Union[typing.List[typing.List[int]], None]]
    game_over: bool


class Turn(BaseModel):
    hand_index: int
    board_position: typing.List[int]


class Hand(BaseModel):
    pieces: typing.List[str]


async def serialize_game(game, response_model=GameState):
    game_over = game.game_over
    score = game.score.score
    board = game.board.board.tolist()
    hand = []
    for piece in game.hand.pieces:
        if piece is None:
            hand.append(None)
        else:
            hand.append(piece.render())

    return {"game_over": game_over, "score": score, "board": board, "hand": hand}


async def deserialize_turn(turn):
    array = np.zeros(103, dtype=np.int8)

    array[turn.hand_index] = 1

    position = turn.board_position[0] * 10 + turn.board_position[1]
    array[position + 3] = 1

    return array


@game_router.get("/new", response_model=GameState)
async def new_game():
    game.new_game()

    return await serialize_game(game)


@game_router.post("/turn", response_model=GameState)
async def submit_turn(turn_body: Turn):
    turn_array = await deserialize_turn(turn_body)
    turn = game.turn_from_array(turn_array)
    game.handle_turn(turn)

    return await serialize_game(game)


@game_router.get("/state", response_model=GameState)
async def game_state():
    return await serialize_game(game)


@game_router.post("/hand", response_model=GameState)
async def submit_hand(hand_body: Hand):
    new_pieces = [PieceFactory.new(piece_name) for piece_name in hand_body.pieces]

    game.replace_hand(new_pieces)
    return await serialize_game(game)


@game_router.get("/save")
async def save_game():
    game.end_game()
