from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.app.server.routes.game import game_router

app = FastAPI()

app.include_router(game_router)

origins = [
    "http://localhost:5173",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
