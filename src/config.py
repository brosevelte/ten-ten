import pathlib

from easydict import EasyDict as edict

from src.mechanics.scoring import rules
from src.mechanics.drawing import RandomSelection

cfg = edict({})
cfg.root_path = pathlib.Path(__file__).parent.parent.resolve()


cfg.game = edict({})

cfg.game.board = edict({})
cfg.game.board.size = 10

cfg.game.scoring = edict({})
cfg.game.scoring.initial_score = 0
cfg.game.scoring.rules = [
    rules.ClearedFullRule(board_size=cfg.game.board.size, points_per_point=1),
    rules.PlacePieceRule(points_per_point=1),
]

cfg.game.hand = edict({})
cfg.game.hand.size = 3
cfg.game.hand.selection = RandomSelection()


cfg.logging = edict({})
cfg.logging.dir = cfg.root_path / "replays"
cfg.logging.type_template = "{agent_id}"
cfg.logging.game_template = "{timestamp}.json"
