class Score:
    def __init__(self, rules, initial_score=0):
        self.score = initial_score
        self.rules = rules

    def handle_turn(self, turn):
        turn.points = 0
        for rule in self.rules:
            turn.points += rule(turn)

        self.score += turn.points

    def __repr__(self):
        return f"{self.__class__.__name__}({self.score})"
