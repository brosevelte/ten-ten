class Rule:
    def __init__(self, points_per_point=1):
        self.points_per_point = points_per_point


class PlacePieceRule(Rule):
    def __call__(self, turn):
        placed_piece = turn.piece
        return len(placed_piece.shape) * self.points_per_point


class ClearedFullRule(Rule):
    def __init__(self, board_size, points_per_point=1):
        super().__init__(points_per_point)

        self.board_size = board_size

    def __call__(self, turn):
        cleared_rows = turn.cleared_rows
        cleared_columns = turn.cleared_columns

        total_cleared = len(cleared_rows) + len(cleared_columns)
        # The first cleared row/col = 10 points, next 20, then 30...
        # We can calculate this using this formula
        triangular_hum = (total_cleared * (total_cleared + 1)) // 2

        return triangular_hum * self.board_size * self.points_per_point
