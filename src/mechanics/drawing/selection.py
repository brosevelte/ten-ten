from src.components.pieces import Piece
from src.chance import rng


class Selection:
    choices = Piece.get_all_pieces()


class RandomSelection(Selection):
    def __call__(self, n: int):
        return rng.gen.choice(self.choices, n, replace=True, p=None)
