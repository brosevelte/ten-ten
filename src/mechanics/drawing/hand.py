from src.mechanics.drawing.selection import Selection


class Hand:
    def __init__(self, n: int, selector: Selection):
        self.n = n
        self.selector = selector

        self.draw()

    def draw(self):
        self.pieces = [piece() for piece in self.selector(self.n)]

    def log_self(self):
        return {
            "type": self.__class__.__name__,
            "pieces": [piece.__class__.__name__ for piece in self.pieces],
        }

    def __getitem__(self, index):
        return self.pieces[index]

    def remove_piece(self, index):
        self.pieces[index] = None

    def __repr__(self):
        return f"Hand(pieces={self.pieces})"

    @property
    def empty(self):
        for piece in self.pieces:
            if piece is not None:
                return False
        return True
