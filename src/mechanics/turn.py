class Turn:
    def __init__(self, origin, piece_index):
        self.origin = origin
        self.piece_index = piece_index

        self.piece = None
        self.was_placed = None
        self.cleared_rows = []
        self.cleared_columns = []
        self.points = 0
        self.message = ""

    def log_self(self):
        return {
            "type": self.__class__.__name__,
            "origin": [int(self.origin.y), int(self.origin.x)],
            "piece_index": int(self.piece_index),
        }

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(origin={self.origin}, hand_index={self.piece_index}, piece={repr(self.piece)}, placed={self.was_placed}, points={self.points}, message='{self.message}')"
