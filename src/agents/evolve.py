import argparse
import logging
import sys

sys.path.append("/home/brosevelte/repos/ten-ten")

import absl.logging

from src.agents.nn_evo.population import Population


def main(size, top_n, num_generations, logger):

    population = Population(size, top_n, logger)

    history = {}
    for generation in range(num_generations):
        best_score = population.run_generation()

        history[generation] = best_score


def log():
    absl.logging.set_verbosity(absl.logging.ERROR)

    logger = logging.getLogger("evolve")
    logger.setLevel(logging.INFO)

    return logger


def parse():
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--size", type=int)
    parser.add_argument("-n", "--top-n", type=int)
    parser.add_argument("-g", "--num-generations", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    args = parse()

    logger = log()

    main(**vars(args), logger=logger)
