from datetime import datetime
import os

import numpy as np
import tensorflow as tf

from src.game import Game
from src.components.pieces import PieceFactory
from src.chance import rng


piece_index_lut = PieceFactory.get_piece_index_lut()


def serialize_game(game):
    board = game.board.board.reshape((100,))
    hand = np.zeros((3, len(piece_index_lut) + 1))

    for i, piece in enumerate(game.hand.pieces):
        if piece is None:
            hand[i][19] = 1
        else:
            hand[i][
                piece_index_lut.get(piece.__class__.__name__, len(piece_index_lut))
            ] = 1

    return np.hstack(
        [
            board,
            hand.reshape(
                60,
            ),
        ]
    ).reshape((1, -1))


class Individual:
    def __init__(self, num_features, timestamp, generation=0, individual=0):
        self.agent_id = "CustomNN_v1"
        self.generation = generation
        self.individual = individual
        self.timestamp = os.path.join(timestamp, f"g{generation}i{individual}")

        self.num_features = num_features
        self.model = self.get_model()

        self.game = Game(self.agent_id, self.timestamp)
        self.log_filename = self.game.get_log_file()

    @property
    def score(self):
        return self.game.score.score

    def __repr__(self):
        return f"Individual(agent_id={self.agent_id}, generation={self.generation}, individual={self.individual}, features={self.num_features})"

    def get_model(self):
        input_layer = tf.keras.Input(shape=(self.num_features,))

        dense_layer = tf.keras.layers.Dense(
            32,
            input_shape=(160,),
            kernel_initializer=tf.keras.initializers.RandomNormal(mean=0.0, stddev=1.0),
            bias_initializer=tf.keras.initializers.RandomUniform(
                minval=-0.05, maxval=0.05
            ),
        )(input_layer)

        placement_layer = tf.keras.layers.Softmax()(dense_layer)
        hand_layer = tf.keras.layers.Softmax()(dense_layer)

        model = tf.keras.Model(input_layer, [placement_layer, hand_layer])
        model.compile(optimizer="sgd", loss="mse")

        return model

    def set_weights(self, weights):
        self.model.set_weights(weights)

    def live(self):
        while not self.game.game_over:
            model_input = serialize_game(self.game)
            model_output = self.model.predict(model_input, verbose=0)

            turn = self.game.turn_from_array(model_output[0][0])

            self.game.handle_turn(turn)

        return self.game.score.score

    def log_self(self):
        self.game.end_game()

        log_dir = self.log_filename.with_suffix("")
        self.model.save(log_dir)


class Breeder:
    def __init__(self):
        # Ideas:
        #   1. Better parent gets more pull in the child
        #   2. Mutations
        #   3. Select genes from parents, do not average
        #   4. Best survive
        pass

    @classmethod
    def __call__(cls, father, mother, timestamp, generation, num):
        father_weights = father.model.get_weights()
        mother_weights = mother.model.get_weights()

        new_weights = []
        for father_layer, mother_layer in zip(father_weights, mother_weights):
            new_weights.append((father_layer + mother_layer) / 2.0)

        child = Individual(mother.num_features, timestamp, generation, num)
        child.set_weights(new_weights)

        return child


class Population:
    def __init__(self, size, top_n, logger):  # breeding_probability):
        self.size = size
        self.top_n = top_n
        self.logger = logger

        self.timestamp = datetime.now().isoformat()

        self.individuals = [Individual(160, self.timestamp, 0, i) for i in range(size)]
        self.best_individuals = []

        self.breeder = Breeder()

        self.current_generation = 0

    def __repr__(self):
        return f"Population(size={self.size}, generation={self.current_generation})"

    def log_life_of(self, individual):
        self.best_individuals.append(individual)
        individual.log_self()

    def run_generation(self):
        for individual in self.individuals:
            individual.live()
        self.current_generation += 1

        # Sort in descending order of score
        scored_individuals = sorted(
            self.individuals, key=lambda individual: -individual.score
        )

        best = scored_individuals[0]
        self.log_life_of(best)
        self.logger.info(
            f"Generation({self.current_generation - 1}: best_generation={best.generation}, best_score={best.score}, num_moves={best.game.total_moves}, num_bad_moves={best.game.bad_moves})"
        )

        self.individuals = self.breeder(scored_individuals)

        return scored_individuals[0].score
