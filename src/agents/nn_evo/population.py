from datetime import datetime
from src.agents.nn_evo.breeding.breeder import Breeder
from src.agents.nn_evo.breeding import mating_methods
from src.agents.nn_evo.breeding import splicing_methods
from src.agents.nn_evo.breeding import mutation_methods
from src.agents.nn_evo.individual import Individual


class Population:
    def __init__(self, size, top_n, logger):
        self.size = size
        self.top_n = top_n
        self.logger = logger

        self.timestamp = datetime.now().isoformat()

        self.individuals = [Individual(160, self.timestamp, 0, i) for i in range(size)]
        self.best_individuals = []

        self.breeder = Breeder(
            self.top_n,
            mating_method=mating_methods.BestMoreLikely(top_n),
            mutation_method=mutation_methods.RandomMutation(0.05),
            splicing_method=splicing_methods.SelectionSplicing(),
        )

        self.current_generation = 0

    def __repr__(self):
        return f"Population(size={self.size}, generation={self.current_generation})"

    def log_life_of(self, individual):
        self.best_individuals.append(individual)
        individual.log_self()

    def run_generation(self):
        for individual in self.individuals:
            individual.live()
        self.current_generation += 1

        # Sort in descending order of score
        scored_individuals = sorted(
            self.individuals, key=lambda individual: -individual.score
        )

        best = scored_individuals[0]
        self.log_life_of(best)
        self.logger.info(
            f"Generation({self.current_generation - 1}: best_generation={best.generation}, best_score={best.score}, num_moves={best.game.total_moves}, num_bad_moves={best.game.bad_moves})"
        )

        self.individuals = self.breeder(
            scored_individuals, self.timestamp, self.current_generation
        )

        return scored_individuals[0].score
