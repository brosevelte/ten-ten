import os

import tensorflow as tf

from src.game import Game


class Individual:
    def __init__(self, num_features, timestamp, generation=0, individual=0):
        self.agent_id = "CustomNN_v1"
        self.generation = generation
        self.individual = individual
        self._timestamp = timestamp
        self.timestamp = os.path.join(timestamp, f"g{generation}i{individual}")

        self.num_features = num_features
        self.model = self.get_model()

        self.game = Game(self.agent_id, self.timestamp)
        self.log_filename = self.game.get_log_file()

    @property
    def score(self):
        return self.game.score.score

    def __repr__(self):
        return f"Individual(agent_id={self.agent_id}, generation={self.generation}, individual={self.individual}, features={self.num_features})"

    def get_model(self):
        input_layer = tf.keras.Input(shape=(self.num_features,))

        dense_layer = tf.keras.layers.Dense(
            32,
            input_shape=(160,),
            kernel_initializer=tf.keras.initializers.RandomNormal(mean=0.0, stddev=1.0),
            bias_initializer=tf.keras.initializers.RandomUniform(
                minval=-0.05, maxval=0.05
            ),
        )(input_layer)

        placement_layer = tf.keras.layers.Softmax()(dense_layer)
        hand_layer = tf.keras.layers.Softmax()(dense_layer)

        model = tf.keras.Model(input_layer, [placement_layer, hand_layer])
        model.compile(optimizer="sgd", loss="mse")

        return model

    def set_weights(self, weights):
        self.model.set_weights(weights)

    def live(self):
        while not self.game.game_over:
            model_input = self.game.serialize_game()
            model_output = self.model.predict(model_input, verbose=0)

            turn = self.game.turn_from_array(model_output[0][0])

            self.game.handle_turn(turn)

        return self.game.score.score

    def log_self(self):
        self.game.end_game()

        log_dir = self.log_filename.with_suffix("")
        self.model.save(log_dir)
