import numpy as np

from src.chance import rng


class MatingMethod:
    def __init__(self, top_k_survive=0):
        self.top_k_survive = top_k_survive

    def get_range(self, num_individuals):
        return range(num_individuals - self.top_k_survive)


class TopNRandom(MatingMethod):
    def __init__(self, top_k_survive, top_n):
        self.top_n = top_n
        super().__init__(top_k_survive)

    def __call__(self, scored_individuals):
        alphas = scored_individuals[: self.top_n + 1]

        pairs = [scored_individuals[: self.top_k_survive]]
        for i in self.get_range(scored_individuals.size):
            parents = rng.gen.choice(alphas, 2, replace=False, p=None)
            pairs.append(parents)

        return pairs


class BestMoreLikely(MatingMethod):
    def __init__(self, top_k_survive):
        super().__init__(top_k_survive)

    def __call__(self, scored_individuals):
        scores = np.array(
            [scored_individual.score for scored_individual in scored_individuals]
        )
        p = scores / scores.sum()

        pairs = scored_individuals[: self.top_k_survive]
        for i in self.get_range(len(scored_individuals)):
            parents = rng.gen.choice(scored_individuals, 2, replace=False, p=p)
            pairs.append(parents)

        return pairs
