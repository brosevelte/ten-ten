import numpy as np

from src.agents.nn_evo.individual import Individual
from src.chance import rng


class SplicingMethod:
    def __call__(self, pairs, timestamp, generation):
        children = []
        for num, pair in enumerate(pairs):
            if isinstance(pair, Individual):
                children.append(pair)
            else:
                children.append(
                    self.splice(pair[0], pair[1], timestamp, generation, num)
                )

        return children


class AverageSplicing(SplicingMethod):
    def splice(self, father, mother, timestamp, generation, num):
        father_weights = father.model.get_weights()
        mother_weights = mother.model.get_weights()

        new_weights = []
        for father_layer, mother_layer in zip(father_weights, mother_weights):
            new_weights.append((father_layer + mother_layer) / 2.0)

        child = Individual(mother.num_features, timestamp, generation, num)
        child.set_weights(new_weights)

        return child


class SelectionSplicing(SplicingMethod):
    """
    Selects a subset of the parents genes with a variable bias to the more
    successful parent.
    """

    def splice(self, father, mother, timestamp, generation, num):
        father_weights = father.model.get_weights()
        mother_weights = mother.model.get_weights()

        total_score = float(father.score + mother.score)
        p = [float(father.score) / total_score, float(mother.score) / total_score]

        new_weights = []
        for father_layer, mother_layer in zip(father_weights, mother_weights):
            mask = rng.gen.choice([0, 1], size=mother_layer.size, p=p).reshape(
                mother_layer.shape
            )

            father_selection = father_layer * mask
            mother_selection = mother_layer * ((mask - 1.0) * -1.0)

            child_weights = father_selection + mother_selection
            new_weights.append(child_weights)

        child = Individual(mother.num_features, timestamp, generation, num)
        child.set_weights(new_weights)

        return child
