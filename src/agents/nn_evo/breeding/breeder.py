class Breeder:
    def __init__(self, top_n, mating_method, mutation_method, splicing_method):
        # Ideas:
        #   1. Better parent gets more pull in the child
        #   2. Mutations
        #   3. Select genes from parents, do not average
        #   4. Best survive
        self.top_n = top_n
        self.mating_method = mating_method
        self.mutation_method = mutation_method
        self.splicing_method = splicing_method

    def __call__(self, scored_individuals, timestamp, generation):
        pairs = self.mating_method(scored_individuals)
        children = self.splicing_method(pairs, timestamp, generation)
        children = self.mutation_method(children)

        return children
