import numpy as np

from src.agents.nn_evo.individual import Individual
from src.chance import rng


class MutationMethod:
    def __init__(self, probability):
        """
        Args
            probability (float): the percent chance a value will be replaced
                by a random weight
        """
        self.probability = probability

    def __call__(self, individuals):
        mutated = []
        for individual in individuals:
            mutated.append(self.splice(individual))

        return mutated


class RandomMutation(MutationMethod):
    def splice(self, individual):
        weights = individual.model.get_weights()

        new_weights = []
        for individual_layer in weights:

            mean_weight = individual_layer.mean()
            std_weight = individual_layer.std()

            mask = np.random.choice(
                [0, 1],
                size=individual_layer.size,
                p=[(1 - self.probability), self.probability],
            ).reshape(individual_layer.shape)

            rand_layer = rng.gen.normal(
                mean_weight, std_weight, size=individual_layer.size
            ).reshape(individual_layer.shape)

            rand_selection = rand_layer * mask
            individual_selection = individual_layer * ((mask - 1) * -1)

            child_weights = rand_selection + individual_selection
            new_weights.append(child_weights)

        child = Individual(
            individual.num_features,
            individual._timestamp,
            individual.generation,
            individual.individual,
        )
        child.set_weights(new_weights)

        return child
