from datetime import datetime
import os
import json

import numpy as np

from src.config import cfg
from src.components import Board, Point
from src.components.pieces import PieceFactory
from src.mechanics.scoring import Score
from src.mechanics.drawing import Hand
from src.mechanics.turn import Turn


class Game:
    def __init__(self, agent_id, timestamp=None):
        self.agent_id = agent_id
        self.new_game(timestamp)

        self.piece_index_lut = PieceFactory.get_piece_index_lut()

    def new_game(self, timestamp=None):
        if timestamp is None:
            self.timestamp = datetime.now().isoformat()
        else:
            self.timestamp = timestamp

        self.board = Board(cfg.game.board.size)
        self.score = Score(cfg.game.scoring.rules, cfg.game.scoring.initial_score)
        self.hand = Hand(cfg.game.hand.size, cfg.game.hand.selection)

        self.bad_moves = 0
        self.total_moves = 0
        self.game_over = False

        self.history = [self.hand.log_self()]

    def end_game(self):
        if len(self.history) == 1:
            return

        log_file = self.get_log_file()
        if not os.path.isdir(log_file.parent):
            os.makedirs(log_file.parent)
        with open(log_file, "w") as log:
            json.dump(self.history, log)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.score}, {self.hand}, is_over={self.game_over})"

    def serialize_game(self):
        board = self.board.board.reshape((100,))
        hand = np.zeros((3, len(self.piece_index_lut) + 1))

        for i, piece in enumerate(self.hand.pieces):
            if piece is None:
                hand[i][19] = 1
            else:
                hand[i][
                    self.piece_index_lut.get(
                        piece.__class__.__name__, len(self.piece_index_lut)
                    )
                ] = 1

        return np.hstack(
            [
                board,
                hand.reshape(
                    60,
                ),
            ]
        ).reshape((1, -1))

    def get_log_file(self):
        subdir = cfg.logging.type_template.format(agent_id=self.agent_id)
        filename = cfg.logging.game_template.format(
            timestamp=self.timestamp.replace(":", "-").replace(".", "-")
        )
        log_path = cfg.logging.dir / subdir / filename
        return log_path

    def replace_hand(self, pieces):
        self.hand.pieces = pieces

    def has_more_moves(self):
        for x in range(self.board.n):
            for y in range(self.board.n):
                for piece in self.hand.pieces:
                    has_move, _ = self.board.can_add_piece(Point(x, y), piece)
                    if has_move:
                        return True

        return False

    def turn_from_array(self, array):
        """
        First 3 elements of array are the piece to place within the hand
        Last 100 elements of the array are the position to plate the piece
        """
        hand_part = array[:3]
        position_part = array[3:]

        piece_index = np.argmax(hand_part)
        # piece = self.hand[piece_index]

        position_num = np.argmax(position_part)
        position_col = position_num // 10
        position_row = position_num % 10

        turn = Turn(Point(position_row, position_col), piece_index)

        return turn

    def handle_turn(self, turn):
        turn.piece = self.hand[turn.piece_index]

        self.total_moves += 1

        can_add_piece, turn.message = self.board.can_add_piece(turn.origin, turn.piece)
        if not can_add_piece:
            self.bad_moves += 1
            if self.bad_moves == 10:
                self.game_over = True
            turn.was_placed = False
            return turn

        # Add the piece to the board
        self.board.add_piece(turn.origin, turn.piece)
        self.hand.remove_piece(turn.piece_index)
        turn.was_placed = True

        # Clear full rows/columns
        turn.cleared_columns = self.board.get_full_columns()
        turn.cleared_rows = self.board.get_full_rows()

        self.board.clear_columns(turn.cleared_columns)
        self.board.clear_rows(turn.cleared_rows)

        # Score the turn
        self.score.handle_turn(turn)

        # Get new pieces if placed three
        drawn = False
        if self.hand.empty:
            self.hand.draw()
            drawn = True

        # End game if can't place anymore pieces
        # TODO: This is super inefficient, could just let run for training
        #       and cap max game time
        if not self.has_more_moves():
            self.game_over = True
            turn.message = "No moves available"

        self.history.append(turn.log_self())
        if drawn:
            self.history.append(self.hand.log_self())

        return turn
