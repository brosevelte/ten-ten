from easydict import EasyDict as edict
import numpy as np

rng = edict({})
rng.seed = 42069
rng.gen = np.random.default_rng(seed=rng.seed)
